'use strict';

/**
 * @ngdoc function
 * @name nanopoolFrontendApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the nanopoolFrontendApp
 */
angular.module('nanopoolFrontendApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $rootScope.currentYear = new Date().getFullYear();
  });
