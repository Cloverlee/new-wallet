'use strict';

/**
 * @ngdoc function
 * @name nanopoolFrontendApp.controller:PayoutsCtrl
 * @description
 * # PayoutsCtrl
 * Controller of the nanopoolFrontendApp
 */
angular.module('nanopoolFrontendApp')
  .controller('PayoutsCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
