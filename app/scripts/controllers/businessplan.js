'use strict';

/**
 * @ngdoc function
 * @name nanopoolFrontendApp.controller:BusinessplanCtrl
 * @description
 * # BusinessplanCtrl
 * Controller of the nanopoolFrontendApp
 */
angular.module('nanopoolFrontendApp')
  .controller('BusinessplanCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
    $rootScope.currentYear = new Date().getFullYear();
  });
