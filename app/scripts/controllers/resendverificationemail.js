'use strict';

/**
 * @ngdoc function
 * @name nanopoolFrontendApp.controller:ResendverificationemailCtrl
 * @description
 * # ResendverificationemailCtrl
 * Controller of the nanopoolFrontendApp
 */
angular.module('nanopoolFrontendApp')
  .controller('ResendverificationemailCtrl', function ($scope, nanopoolService, config) {

    $scope.s3Url = config.s3BucketUrl;
    $scope.emailError = false;

    // Forgot Password
    $scope.submit = function() {
      if($scope.user) {
        $scope.emailError = false;
        $scope.emailId = JSON.stringify($scope.user.email);
        nanopoolService.resendEmail($scope.emailId)
          .then(function(res){
            if(res.status === 200) {
              $scope.hasSuccess = true;
              $scope.successEmail = '<'+$scope.user.email+'>';
            }else if(res.status === 404){
              $scope.resendError = res.data.Message;
            }else{
              $scope.resendError = 'OOPS! Something went wrong. Please try again.';
            }
          })
      }else{
        $scope.emailError = true;
      }
    }
  });
