'use strict';

/**
 * @ngdoc function
 * @name nanopoolFrontendApp.controller:TotalsignupCtrl
 * @description
 * # TotalsignupCtrl
 * Controller of the nanopoolFrontendApp
 */
angular.module('nanopoolFrontendApp')
  .controller('TotalsignupCtrl', function ($scope, nanopoolService, UtilsService) {
    // Get Country Codes
    UtilsService.getCountryCode().then(function(res) {
      $scope.allCountryCodes = res;
    });

    // Get Total Signups
    nanopoolService.getLatestSignup().then(function(res){
      if(res.status === 200) {
        $scope.totalUsers = res.data.totalusers;
        $scope.latestSignup = res.data.data;
      }
    })
  });
