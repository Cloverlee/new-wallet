'use strict';

/**
 * @ngdoc function
 * @name nanopoolFrontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the nanopoolFrontendApp
 */
angular.module('nanopoolFrontendApp')
  .controller('MainCtrl', function ($scope, $rootScope, $cookies, $state, $window, $location, nanopoolService, config) {

    $rootScope.s3Url = config.s3BucketUrl;
    $rootScope.isLoggedIn = false;

    // Authenticate User
    if($location.path() !== '/terms-and-conditions' && nanopoolService.isAuthenticated){
      $state.go('dashboard');
    }

  });
