'use strict';

/**
 * @ngdoc function
 * @name nanopoolFrontendApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the nanopoolFrontendApp
 */
angular.module('nanopoolFrontendApp')
  .controller('HeaderCtrl', function ($scope, $sce, $cookies, $state, $rootScope, $localStorage, $timeout, $uibModal, $uibModalStack, nanopoolService, UtilsService, config, tawkToService, TAWKTO_ID) {
    $rootScope.s3Url = config.s3BucketUrl;
    $rootScope.currentYear = new Date().getFullYear();
    $scope.activeMenu = $state.current.name;
    $rootScope.signin = false;
    //  Logged out User
    $scope.logout = function () {
      $localStorage.$reset();
      $cookies.remove('token');
      $cookies.remove('openTimerPopup');
      $state.go('login');
      $window.location.reload();
    }

  $scope.getClass = function(path) {
    $scope.activeMenu = path;
    // return ($state.includes(path)) ? 'highlite' : '';
  }
  $scope.closeMenu = function() {
    angular.element(document.querySelector("#bs-example-navbar-collapse-1")).removeClass('in');
  }

    // // Calcultate remaining Time
    // var futureDate = moment.utc('2016-11-14 09:00:00');
    // var currentDate = moment.utc();
    // $rootScope.remainingTime = futureDate.diff(currentDate, 'seconds');

    $rootScope.tokenRequestProgress = false;

    $rootScope.$on('getRefreshToken', function () {
      if ($localStorage.refresh_token && !$cookies.get('token') && !$rootScope.tokenRequestProgress) {
        $rootScope.tokenRequestProgress = true;
        var refreshTokenParams = {
          'grant_type': 'refresh_token',
          'refresh_token': $localStorage.refresh_token
        }

        nanopoolService.getRefreshToken(refreshTokenParams).then(function (res) {
          var data = res.data;
          if (res.status === 200) {
            $localStorage.$reset();
            $cookies.put('token', data.access_token, { expires: moment().second(data.expires_in).toISOString() });
            $localStorage.$default({ token: data.access_token, expires: moment().second(data.expires_in).toISOString(), refresh_token: data.refresh_token });
          } else {
            $rootScope.$broadcast('logout');
          }
          $rootScope.tokenRequestProgress = false;
        });
      } else if (!$localStorage.refresh_token) {
        $rootScope.$broadcast('logout');
      }
    });

    //Get User Info
    $scope.getUserDetails = function () {
      nanopoolService.getUserInfo().then(function (res) {
        var data = res.data;
        if (res.status === 200) {
          $rootScope.userInfo = data;
          $rootScope.name = data.name;
          $rootScope.sponsorId = data.username;
          $rootScope.country = data.Country;
          $rootScope.autorotator = data.autorotator;
          $rootScope.landingPage = data.LandingPage;

          $rootScope.$broadcast('userDetails', $rootScope.userInfo);

          if (data.sponsor === "0") {
            $rootScope.parentSponsor = false;
          } else {
            $rootScope.parentSponsor = true;
          }

          if ($rootScope.autorotator === 'L') {
            $rootScope.activePlacement = 'Left Placement';
          } else if ($rootScope.autorotator === 'R') {
            $rootScope.activePlacement = 'Right Placement';
          } if ($rootScope.autorotator === 'A') {
            $rootScope.activePlacement = 'Alternate Placement';
          } if ($rootScope.autorotator === 'W') {
            $rootScope.activePlacement = 'Balance Weak Leg';
          }
          $scope.getFlag($rootScope.country);
        }
        else if (res.status == 401) {
          $state.go('login');
        }
      });
    }

    $scope.getUserDetails();

    $scope.getFlag = function (countryName) {
      UtilsService.getCountryFlag(countryName).then(function (res) {
        $scope.countryFlag = res[0];
      });
    }

    // Get Country Codes
    UtilsService.getCountryCode().then(function (res) {
      $rootScope.allCountryCodes = res;
    });

    // Get Total Signups
    nanopoolService.getLatestSignup().then(function (res) {
      if (res.status === 200) {
        //CPTiwari
        $rootScope.totalUsers = res.data.totalusers;
        $rootScope.latestSignup = res.data.data;
        $rootScope.latestSignup.forEach(function (_info, key) {
          _info.DOJ += 'Z';
        });
      }
    })

    $scope.videoOne = config.videoIdOne;
    $scope.videoTwo = config.videoIdTwo;
    $scope.playerAttr = {
      autoplay: true
    };

    // Open Video Modal
    // $scope.openVideo = function() {
    //   var url = config.academyUrl;
    //   var autoplay = true;
    //   $scope.videoUrl = $sce.trustAsResourceUrl(url+autoplay);
    //   var modalInstance = $uibModal.open({
    //       templateUrl: 'views/modal/cloud-mining-video.html',
    //       scope: $scope,
    //       size: 'lg',
    //       windowClass: 'academy-video'
    //   });
    // }
    // $scope.closePopup = function() {
    //   $uibModalStack.dismissAll();
    // }

    var vm = this;
    //YOUR TAWK.TO ID GOES HERE
    vm.id = TAWKTO_ID;
    vm.visitor = {};
    vm.loaded = false;
    vm.currentUser = null;

    function setVisitor() {
      if (nanopoolService.isAuthenticated()) {
        vm.visitor = tawkToService.setVisitor($rootScope.name, $rootScope.email);
      }
      else {
        vm.visitor = tawkToService.setVisitor('ngTawkTo Demo User', 'demo@demo.com');
      }
    }

    $scope.$on('TAWKTO:onLoad', function () {
      $scope.$apply(setVisitor);
      vm.loaded = true;
    });
  });
