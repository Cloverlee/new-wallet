'use strict';

/**
 * @ngdoc function
 * @name nanopoolFrontendApp.controller:ForgotpasswordCtrl
 * @description
 * # ForgotpasswordCtrl
 * Controller of the nanopoolFrontendApp
 */
angular.module('nanopoolFrontendApp')
  .controller('ForgotpasswordCtrl', function ($scope,nanopoolService,config) {

    $scope.s3Url = config.s3BucketUrl;
    $scope.userIdError = false;

    // Forgot Password
    $scope.submit = function() {
      if($scope.user) {
        $scope.userIdError = false;
        $scope.userId = JSON.stringify($scope.user.userId);
        nanopoolService.forgotPassword($scope.userId)
          .then(function(res){
            if(res.status === 200) {
              $scope.hasSuccess = true;
              $scope.successUserId = '<'+$scope.user.userId+'>';
            }
          })
      }else{
        $scope.userIdError = true;
      }
    }

  });
