'use strict';

/**
 * @ngdoc directive
 * @name nanopoolFrontendApp.directive:purchaseTotal
 * @description
 * # purchaseTotal
 */
angular.module('nanopoolFrontendApp')
  .directive('purchaseTotal', function () {
    return {
      restrict: 'A',
      templateUrl: 'views/directiveTemplates/purchaseTotal.html',
      scope: {
        purchasedAttr: "=",
        purchasedParams: "=",
        totalAttr: "=",
        totalParams: "=",
        s3Url: "="
      }
    };
  });
