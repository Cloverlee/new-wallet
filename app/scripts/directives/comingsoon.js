'use strict';

/**
 * @ngdoc directive
 * @name nanopoolFrontendApp.directive:comingSoon
 * @description
 * # comingSoon
 */
angular.module('nanopoolFrontendApp')
  .directive('comingSoon', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/directiveTemplates/comingSoon.html',
      scope:{
        textValue: "=",
        textAttr: "="
      }
    };
  });
