'use strict';

/**
 * @ngdoc directive
 * @name nanopoolFrontendApp.directive:texteditor
 * @description
 * # texteditor
 */
angular.module('nanopoolFrontendApp')
  .directive('texteditor', function () {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        $(element).Editor();
      }
    };
  });
