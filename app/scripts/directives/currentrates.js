'use strict';

/**
 * @ngdoc directive
 * @name nanopoolFrontendApp.directive:currentRates
 * @description
 * # currentRates
 */
angular.module('nanopoolFrontendApp')
  .directive('currentRates', function () {
    return {
      restrict: 'A',
      templateUrl: 'views/directiveTemplates/currentRates.html',
      scope: {
        currentMining: "=",
        miningParams: "=",
        s3Url: "="
      }
    };
  });
